using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{

    public int itemsCollected;
    public int requiredItems;
    public GameObject scoreUI;
    [SerializeField] GameObject LevelCompleteUI;
    [SerializeField] AudioSource VictorySound;
    [SerializeField] AudioSource ItemPickupSound;

    void Start()
    {
        if(SceneManager.GetActiveScene().buildIndex == 1)
        {
            requiredItems = 3;
        }
        else if (SceneManager.GetActiveScene().buildIndex == 2)
        {
            requiredItems = 5;
        }
        scoreUI = GameObject.Find("Score");
    }

    void Update()
    {
        scoreUI.GetComponent<TMPro.TextMeshProUGUI>().text = "Collected" + itemsCollected.ToString() + " / " + requiredItems;

        if(itemsCollected == requiredItems)
        {
            LevelComplete();
        }
    }

    public void LevelComplete()
    {
        Time.timeScale = 0;
        VictorySound.Play();
        GameObject.FindWithTag("MainCamera").GetComponent<FirstPersonLook>().enabled = false;
        Cursor.lockState = CursorLockMode.None;
        LevelCompleteUI.SetActive(true);
    }

    public void Home(int sceneID)
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene(sceneID);
        LevelCompleteUI.SetActive(false);
    }

    public void LoadLevel(int sceneID)
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene(sceneID);
        LevelCompleteUI.SetActive(false);
    }
}
