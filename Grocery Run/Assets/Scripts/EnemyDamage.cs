using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDamage : MonoBehaviour
{
    public GameOver player;
    private void OnTriggerEnter(Collider collider)
    {

        if(collider.name == "shopping cart")
        {
            Destroy(collider);
            player.EndGame = true;
            player.StopGame();
        }
    }
}
