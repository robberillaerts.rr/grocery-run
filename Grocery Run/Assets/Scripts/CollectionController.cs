using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectionController : MonoBehaviour
{
    public GameManager gm = new GameManager();

    void OnTriggerEnter(Collider collider)
    {
        if(collider.name == "shopping cart")
        {
            GameObject.Find("Item Pickup Sound").GetComponent<AudioSource>().Play();
            Destroy(this.gameObject);
            gm.itemsCollected++;
        }
    }
}
