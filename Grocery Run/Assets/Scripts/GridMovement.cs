using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridMovement : MonoBehaviour
{

    private bool isMoving;
    private Quaternion origRot, targetRot;
    private float timeToMove = 1;
    public float xVel = 0;
    public float yVel = 0;
    public float zVel = 5;

    void Update()
    {
        if (Input.GetKey(KeyCode.A) && !isMoving)
        {
            isMoving = true;
            targetRot = new Quaternion(0, 1, 0, 0);
            while(transform.rotation != targetRot)
            {
                transform.Rotate(0, -1, 0, 0);
            }

            isMoving = false;
        }

        if (Input.GetKey(KeyCode.D))
        {
            isMoving = true;
            transform.Rotate(0, 1, 0, 0);
            isMoving = false;
        }

        GetComponent<Rigidbody>().velocity = new Vector3(xVel, yVel, zVel);
    }
    
}
