using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOver : MonoBehaviour
{
    public bool EndGame;

    void Start()
    {
        EndGame = false;
        Time.timeScale = 1;
    }

    [SerializeField] GameObject GameOverMenuUI;

    void Update()
    {
        if(EndGame == true)
        {
            StopGame();
        }
    }

    public void StopGame()
    {
        Cursor.lockState = CursorLockMode.None;
        Time.timeScale = 0;
        GameOverMenuUI.SetActive(true);
        GameObject.FindWithTag("MainCamera").GetComponent<FirstPersonLook>().enabled = false;
    }

    public void RestartLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void ReturnToMenu()
    {
        Time.timeScale = 1;
        Debug.Log("Menu pressed");
        SceneManager.LoadScene("MainMenu");
    }
}
