using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioController : MonoBehaviour
{
    [SerializeField] AudioSource sound;
    Vector3 lastPos;
    [SerializeField] GameObject player;
    bool check;

    void Update()
    {
        if(player.GetComponent<Rigidbody>().velocity.magnitude >= 0.15)
        {
            if (!sound.isPlaying)
            {
                sound.Play();
            }
            check = true;
        }
        if (player.GetComponent<Rigidbody>().velocity.magnitude <= 0.15 && check == true)
        {
            StartCoroutine(FadeOut(sound, 0.2f));

            check = false;
        }
        if (Time.timeScale == 0)
        {
            sound.Stop();
        }
    }

    public static IEnumerator FadeOut(AudioSource audioSource, float FadeTime)
    {
        float startVolume = audioSource.volume;

        while (audioSource.volume > 0)
        {
            audioSource.volume -= startVolume * Time.deltaTime / FadeTime;

            yield return null;
        }

        audioSource.Stop();
        audioSource.volume = startVolume;
    }

}
