using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemFlee : MonoBehaviour
{
    public float speed;
    public Vector3 direction;
    public Transform fleeFrom;
    public float fleeDistance = 5;
    public float wanderRadius = 3f;
    public UnityEngine.AI.NavMeshAgent agent;
    private Vector3 wanderPoint;

    void Update()
    {
        if (Vector3.Distance(transform.position, fleeFrom.position) < fleeDistance)
        {
            Vector3 newDirection = transform.position - fleeFrom.position;
            newDirection = new Vector3(newDirection.x, 0, newDirection.z);
            newDirection = newDirection.normalized;
            direction = newDirection;
        }
        else
        {
            Wander();
        }
        transform.position += speed * direction * Time.deltaTime;
    }

    public void Wander()
    {
        wanderPoint = RandomWanderPoint();
        if (Vector3.Distance(transform.position, wanderPoint) < 10f)
        {
            wanderPoint = RandomWanderPoint();
        }
        else
        {
            agent.SetDestination(wanderPoint);
        }
    }

    public Vector3 RandomWanderPoint()
    {
        Vector3 randomPoint = (Random.insideUnitSphere * wanderRadius) + transform.position;
        UnityEngine.AI.NavMeshHit navHit;
        UnityEngine.AI.NavMesh.SamplePosition(randomPoint, out navHit, wanderRadius, -1);
        return new Vector3(navHit.position.x, transform.position.y, navHit.position.z);
    }
}
